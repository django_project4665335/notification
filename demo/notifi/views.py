from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from .forms import RegistrationForm, LoginForm
from .models import *
import requests
from django.views.decorators.csrf import csrf_exempt
# from notifi.signals import *
from django.contrib import messages


def create_onesignal_player(user_uuid):
    onesignal_app_id = '82f8da3a-9003-4343-9db3-fd6543c32655'
    onesignal_api_key = 'ZTc1YzQ4ZTgtMGU2OC00NGY3LThjMzMtMTA3NDdmNTU0ZDE1'

    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Basic {onesignal_api_key}',
    }

    data = {
        'app_id': onesignal_app_id,
        'device_type': 3,
        'identifier': str(user_uuid),
    }

    response = requests.post('https://onesignal.com/api/v1/players', json=data, headers=headers)

    if response.status_code == 200:
        response_json = response.json()
        subscription_id = response_json.get('id')
        
        if subscription_id:
            print(f'OneSignal player created successfully for user {user_uuid}. Subscription ID: {subscription_id}')
            return subscription_id
        else:
            print(f'Failed to retrieve subscription ID from OneSignal response: {response.text}')
            return None
    else:
        print(f'Failed to create OneSignal player. Response: {response.text}')
        return None

def send_push_notification(subscription_id, message):
    onesignal_app_id = '82f8da3a-9003-4343-9db3-fd6543c32655'
    onesignal_api_key = 'ZTc1YzQ4ZTgtMGU2OC00NGY3LThjMzMtMTA3NDdmNTU0ZDE1'

    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Basic {onesignal_api_key}',
    }

    data = {
        'app_id': onesignal_app_id,
        'device_type': 3,   
        'include_player_ids': [subscription_id],
    }

    try:
        response = requests.post('https://onesignal.com/api/v1/notifications', json=data, headers=headers)
        response.raise_for_status()

        response_json = response.json()

        if 'errors' in response_json:
            print(f'Error sending push notification: {response_json["errors"]}')
            return False
        elif 'id' in response_json:
            print(f'Push notification sent successfully with notification ID {response_json["id"]}')
            return True
        else:
            print(f'Unexpected response: {response_json}')
            return False
    except requests.RequestException as e:
        print(f'Error sending push notification: {e}')
        return False

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            
            subscription_id = create_onesignal_player(user.uuid)
            if subscription_id is None:
                return redirect('register')
            
            user.subscription_id = subscription_id
            user.save()
            return redirect('login')
    else:
        form = RegistrationForm()
    return render(request, 'notifi/register.html', {'form': form})

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request, request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('home')
    else:
        form = LoginForm()
    return render(request, 'notifi/login.html', {'form': form})

def home(request):
    if request.user.is_authenticated:
        subscription_id = request.user.subscription_id

        message = "Hello, this is a push notification!"
        send_push_notification(subscription_id, message)

    return render(request, 'notifi/home.html', {'push_notification_message': message})

def user_logout(request):
    logout(request)
    return redirect('login')














# Send a welcome push notification
# if not send_push_notification(subscription_id, message='Welcome to our app!'):
#     return redirect('register')


# def send_push_notification(user_uuid, message='Welcome to our app!'):
#     onesignal_app_id = '82f8da3a-9003-4343-9db3-fd6543c32655'
#     onesignal_api_key = 'ZTc1YzQ4ZTgtMGU2OC00NGY3LThjMzMtMTA3NDdmNTU0ZDE1'

#     if not is_user_subscribed(user_uuid):
#         print(f'User {user_uuid} is not subscribed to push notifications. Skipping notification.')
#         return

#     headers = {
#         'Content-Type': 'application/json',
#         'Authorization': f'Basic {onesignal_api_key}',
#     }

#     data = {
#         'app_id': onesignal_app_id,
#         'contents': {'en': message},
#         'include_player_ids': [str(user_uuid)],
#     }

#     print(f'Sending push notification to user {user_uuid} with message: "{message}"')

#     response = requests.post('https://onesignal.com/api/v1/notifications', json=data, headers=headers)

#     print(response.text)
    
#     if response.status_code == 200:
#         print('Push notification sent successfully!')
#     else:
#         print(f'Failed to send push notification. Response: {response.text}')

#     return response.status_code














# import json
# import requests
# from django.shortcuts import render, redirect
# from django.contrib.auth import authenticate, login, logout
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.forms import AuthenticationForm
# from .forms import RegisterForm

# ONESIGNAL_APP_ID = '82f8da3a-9003-4343-9db3-fd6543c32655'
# ONESIGNAL_REST_API_KEY = 'ZTc1YzQ4ZTgtMGU2OC00NGY3LThjMzMtMTA3NDdmNTU0ZDE1'

# @login_required
# def home(request):
#     user_id = request.user.id
#     message_title = "Welcome to My Website"
#     message_content = "Thank you for visiting our website. Stay tuned for updates!"

#     # Send a notification to the user
#     send_onesignal_notification(user_id, message_title, message_content)

#     return render(request, 'notifi/home.html')

# def send_onesignal_notification(user_id, message_title, message_content):
#     onesignal_url = 'https://onesignal.com/api/v1/notifications'
#     headers = {
#         'Content-Type': 'application/json',
#         'Authorization': f'Basic {ONESIGNAL_REST_API_KEY}'
#     }
#     payload = {
#         'app_id': ONESIGNAL_APP_ID,
#         'include_player_ids': [str(user_id)],
#         'headings': {'en': message_title},
#         'contents': {'en': message_content},
#     }
#     response = requests.post(onesignal_url, headers=headers, data=json.dumps(payload))

#     if response.status_code == 200 and response.json().get('id'):
#         print("Notification sent successfully!")
#     elif response.status_code == 200 and 'errors' in response.json():
#         print(f'Failed to send notification. Errors: {response.json()["errors"]}')
#     else:
#         error_message = f'Failed to send notification. Response: {response.text}'
#         print(error_message)

#     return response

# def subscribe_user_to_onesignal(user_id):
#     onesignal_url = 'https://onesignal.com/api/v1/players'
#     headers = {
#         'Content-Type': 'application/json',
#         'Authorization': f'Basic {ONESIGNAL_REST_API_KEY}'
#     }
#     payload = {
#         'app_id': ONESIGNAL_APP_ID,
#         'identifier': str(user_id),
#         'device_type': 3,
#     }
#     response = requests.post(onesignal_url, headers=headers, data=json.dumps(payload))

#     if response.status_code == 200 and response.json().get('success', False):
#         print("User subscribed to OneSignal successfully!")
#     elif response.status_code == 200 and 'errors' in response.json():
#         print(f'Failed to subscribe user. Errors: {response.json()["errors"]}')
#     else:
#         error_message = f'Failed to subscribe user. Response: {response.text}'
#         print(error_message)

#     return response

# def register(request):
#     if request.method == 'POST':
#         form = RegisterForm(request.POST)
#         if form.is_valid():
#             user = form.save()

#             # Subscribe user to OneSignal
#             onesignal_response = subscribe_user_to_onesignal(user.id)

#             # Check if the subscription was successful
#             if onesignal_response.status_code == 200 and onesignal_response.json().get('success', False):
#                 return redirect('login')
#             else:
#                 error_message = f'Failed to subscribe to OneSignal. Response: {onesignal_response.json()}'
#                 return render(request, 'notifi/register.html', {'error_message': error_message})
#     else:
#         form = RegisterForm()

#     return render(request, 'notifi/register.html', {'form': form})

# def user_login(request):
#     if request.method == 'POST':
#         form = AuthenticationForm(request, data=request.POST)
#         if form.is_valid():
#             user = authenticate(request, username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password'))
#             if user is not None:
#                 login(request, user)
#                 return redirect('home')
#             else:
#                 messages.error(request, 'Invalid username or password.')
#     else:
#         form = AuthenticationForm()
#     return render(request, 'notifi/login.html', {'form': form})

# @login_required
# def user_logout(request):
#     logout(request)
#     return redirect('login')
