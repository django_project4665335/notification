from django.db import models
import uuid
from django.contrib.auth.models import AbstractUser


class UserProfile(AbstractUser):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    subscription_id = models.CharField(max_length=255, blank=True, null=True)

class Post(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField()

    def __str__(self):
        return self.title






# class User(AbstractUser):

#     email = models.EmailField(unique=True)


#     def __str__(self):
#         return self.username