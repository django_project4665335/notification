from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import UserProfile

class RegistrationForm(UserCreationForm):
    class Meta:
        model = UserProfile
        fields = ('username', 'password1', 'password2')

class LoginForm(AuthenticationForm):
    pass



# from django import forms
# from django.contrib.auth.forms import UserCreationForm
# from notifi.models import User

# class RegisterForm(UserCreationForm):

#     class Meta:
#         model = User
#         fields = ['username', 'email', 'password1', 'password2']

