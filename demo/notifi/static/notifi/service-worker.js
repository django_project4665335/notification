// service-worker.js

self.addEventListener('push', function(event) {
    var options = {
        body: event.data.text(),
    };

    event.waitUntil(
        self.registration.showNotification('Push Notification', options)
    );
});

self.addEventListener('notificationclick', function(event) {
    event.notification.close();
});
